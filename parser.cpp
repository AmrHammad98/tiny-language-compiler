#include "parser.h"

#include<QString>

TokenRecord token;               //assigned by calling the function getToken

bool ms = false;
bool endf = false;

int startX = 720  , startY = 23;

QList<TokenRecord> tokenlist;
QList<QString> errorlist;

QList<QString> statements;
QList<QString> expressions;

QString of;

QVector<QRect> rectangles;
QVector<QRect> circles;
QVector<QLine> lines;

syntaxTree *tree;

QString getnodeType(treeNode * node)
{
    if(node->ntype == stmt)
    {
        switch (node->stype)
        {
            case ifStmt :
                return "IF";
            case writeStmt :
                return "Write";
            case repeatStmt :
                return "Repeat";
            case assignStmt :
                return "Assign";
            case readStmt:
                return "Read";
        }
    }
    else if (node->ntype == expr)
    {
        switch (node->etype)
        {
            case op :
                return "OP";
            case constant :
                return "Const";
            case identifier :
                return "ID";
        }
    }
    return "none";
}

void Traverse(treeNode * node)
{
    if (node != nullptr)
    {

        if (node->ntype == stmt)
        {
            statements.append(getnodeType(node));
            of.append("Statement type : ");
            of.append(getnodeType(node));
            of.append("\n");

            if (endf == false)
            {
                endf = true;

                node->x = startX;
                node->y = startY;

                QRect rect ( node->x, node->y , 50 , 50);
                rectangles.append(rect);
            }
            else
            {
                QRect rect ( node->x, node->y , 50 , 50);
                rectangles.append(rect);
            }

        }
        else if (node->ntype == expr)
        {
            expressions.append(getnodeType(node));
            of.append("Expression type : ");
            of.append(getnodeType(node));
            of.append("\n");

            if (endf == false)
            {
                endf = true;

                node->x = startX;
                node->y = startY;

                QRect rect ( node->x, node->y , 40 , 40);
                circles.append(rect);
            }
            else
            {
                QRect rect ( node->x, node->y , 40 , 40);
                circles.append(rect);
            }
        }

        if (node->children[0] != nullptr)
        {
            if(node->stype == assignStmt || node->stype == writeStmt )
            {
                node->children[0]->x = node->x+15;
                node->children[0]->y = node->y+70;
            }
            else if (node->etype == op)
            {

                node->children[0]->x = node->x-10;
                node->children[0]->y = node->y+65;
            }
            else
            {
                node->children[0]->x = node->x-50;
                node->children[0]->y = node->y+115;
            }


            QLine line(node->x+25,node->y+50,node->children[0]->x+25,node->children[0]->y);
            lines.append(line);
        }



        Traverse(node->children[0]);



        if (node->children[1] != nullptr)
        {
            if (node->etype == op)
            {

                node->children[1]->x = node->x+55;
                node->children[1]->y = node->y+65;
            }
            else
            {
                node->children[1]->x = node->x+120;
                node->children[1]->y = node->y+115;

            }


            QLine line1(node->x+25,node->y+50,node->children[1]->x+25,node->children[1]->y);
            lines.append(line1);
        }

        Traverse(node->children[1]);

        if (node->children[2] != nullptr)
        {
            node->children[2]->x = node->x+200;
            node->children[2]->y = node->y+115;


            QLine line2(node->x+25,node->y+50,node->children[2]->x+25,node->children[2]->y);
            lines.append(line2);
        }


        Traverse(node->children[2]);

        if (node->sibling != nullptr)
        {
            node->sibling->x = node->x + 100 ;
            node->sibling->y = node->y;

            QLine lineS(node->x+50,node->y+25,node->sibling->x,node->sibling->y+25);
            lines.append(lineS);
        }

        Traverse(node->sibling);
    }
}

//function to create syntax tree
syntaxTree * createTree(void)
{
    syntaxTree* Tree;
    Tree = (syntaxTree*)malloc(sizeof(syntaxTree));
    if (Tree == nullptr)
    {
        cout << "ERROR" << endl;
    }
    Tree->root = (treeNode*)malloc(sizeof *Tree->root);
    if (Tree->root == nullptr)
    {
        cout << "ERROR" << endl;
    }
    return Tree;
}


void error(TokenType tt ,int i )
{
   if (i == 1)
   {
       QString e = "syntax error expected ";
       e.append(getTokenType(tt));
       e.append("\n");
       //cout<<e.toStdString();
       errorlist.append(e);
   }
   else if (i == 2)
   {
       QString e = "syntax error unexpected token  ";
       e.append(getTokenType(tt));
       e.append("\n");
       //cout<<e.toStdString();
       errorlist.append(e);
   }

}

//match function
bool match(TokenType ttype)
{
    if (token.type == ttype)
    {
        token = getToken();
        tokenlist.append(token);
        return true;
    }
    else
    {
        error(ttype ,1);
        return false;
    }
}

//EBNF implementation

//program function
syntaxTree * Program(void)
{
    syntaxTree * temp;

    token = getToken();

    temp = stmtSequence();

    return temp;
}

//statement sequence function
syntaxTree * stmtSequence(void)
{
    syntaxTree *temp,*temp1,*newtemp;

    temp = statement();
    temp1 = temp;

    while ((token.type != END) && (token.type != ELSE) && (token.type != UNTIL) && /*end of file*/(token.type != eof) )
    {
        //token = getToken();
        if (ms == false)
        {
            match(SEMIC);
        }
        ms = false;

        //token = getToken();
        newtemp = statement();

        if (newtemp != nullptr)
        {
            //case of no more statements are left
            if (temp == nullptr)
            {
                temp = temp1 = newtemp;
            }
            else
            {
                if (temp1 != nullptr && newtemp != nullptr)
                {
                    if (temp1->root != nullptr && newtemp->root != nullptr)
                    {
                        temp1->root->sibling = newtemp->root;
                        temp1 = newtemp;
                    }
                }

                //if not end of file
                if (token.type != eof )
                {
                    match(SEMIC);
                    ms = true;
                }

            }
        }
    }   
    return temp;
}

//statement function
syntaxTree * statement(void)
{
    syntaxTree * temp = nullptr;

    switch (token.type)
    {
    case IF:
        temp = ifStatement();
        break;
    case REPEAT:
        temp = repeatStatement();
        break;
    case READ:
        temp = readStatement();
        break;
    case WRITE:
        temp = writeStatement();
        break;
    case ID:
        temp = assignStatament();
        break;
    default:
        error(token.type ,2);
        token = getToken();
        break;
    }
    return temp;
}

//if statement function
syntaxTree * ifStatement(void)
{
    syntaxTree * temp;

    //token = getToken();
    match(IF);

    //create an if statement node
    temp = createTree();
    temp->root->ntype = stmt;
    temp->root->stype = ifStmt;
    temp->root->children[0] = nullptr;
    temp->root->children[1] = nullptr;
    temp->root->children[2] = nullptr;
    temp->root->sibling = nullptr;

    //test part
    if (temp != nullptr)
    {
        //to avoid nullptr ex can be removed an replace t with expression
        syntaxTree * t = expression();
        if (t != nullptr)
        {
            if (t->root != nullptr)
            {
                temp->root->children[0] = t->root;
            }
        }
    }

    match(THEN);
    /*if (match(THEN) == false)
    {
        token = getToken();
        //match(token.type);
    }*/


    //then part
    if (temp != nullptr)
    {
        //to avoid nullptr ex can be removed an replace t with stmtSequence
        syntaxTree * t = stmtSequence();
        if (t != nullptr)
        {
            if (t->root != nullptr)
            {
                temp->root->children[1] = t->root;
            }
        }
    }

    //else part
    if (token.type == ELSE)
    {
        match(ELSE);
        if (temp != nullptr)
        {
            //to avoid nullptr ex can be removed an replace t with stmtSequence
            syntaxTree * t = stmtSequence();
            if (t != nullptr)
            {
                if (t->root != nullptr)
                {
                    temp->root->children[2] = t->root;
                }
            }

        }
    }
    else
    {
        temp->root->children[2] = nullptr;
    }

    //token = getToken();
    match(END);

    return temp;
}

//repeat statement function
syntaxTree * repeatStatement(void)
{
    syntaxTree * temp;


    match(REPEAT);

    //create a repeat node
    temp = createTree();
    temp->root->ntype = stmt;
    temp->root->stype = repeatStmt;
    temp->root->children[0] = nullptr;
    temp->root->children[1] = nullptr;
    temp->root->children[2] = nullptr;
    temp->root->sibling = nullptr;

    //body part
    if (temp != nullptr)
    {
        syntaxTree * t = stmtSequence();
        if (t != nullptr)
        {
            if (t->root != nullptr)
            {
                temp->root->children[0] = t->root;
            }
        }
    }

    //token = getToken();

    match(UNTIL);

    //test part
    if (temp != nullptr)
    {
        syntaxTree * t = expression();
        if (t!= nullptr && temp!= nullptr)
        {
            if (t->root != nullptr && temp->root != nullptr)
            {
                temp->root->children[1] = t->root;
            }
        }
    }
    return temp;
}

//assignment statement function
syntaxTree * assignStatament(void)
{
    syntaxTree * temp;

    match(ID);

    match(ASSIGN);

    //create an assign node
    temp = createTree();
    temp->root->ntype = stmt;
    temp->root->stype = assignStmt;
    temp->root->children[0] = nullptr;
    temp->root->children[1] = nullptr;
    temp->root->children[2] = nullptr;
    temp->root->sibling = nullptr;

    //expression part
    if (temp != nullptr)
    {
        syntaxTree * t = expression();
        if (t != nullptr)
        {
            if (t->root != nullptr)
            {
                temp->root->children[0] = t->root;
            }
        }

    }
    return temp;
}

//read statement function
syntaxTree * readStatement(void)
{
    syntaxTree * temp;

    match(READ);

    //create a read statement node
    temp = createTree();
    temp->root->ntype = stmt;
    temp->root->stype = readStmt;
    temp->root->children[0] = nullptr;
    temp->root->children[1] = nullptr;
    temp->root->children[2] = nullptr;
    temp->root->sibling = nullptr;

    match(ID);

    return temp;
}

//write statement function
syntaxTree * writeStatement(void)
{
    syntaxTree * temp;

    match(WRITE);

    //create a write node
    temp = createTree();
    temp->root->ntype = stmt;
    temp->root->stype = writeStmt;
    temp->root->children[0] = nullptr;
    temp->root->children[1] = nullptr;
    temp->root->children[2] = nullptr;
    temp->root->sibling = nullptr;

    //expresion part
    if (temp != nullptr)
    {
        syntaxTree * t = expression();
        if (t != nullptr)
        {
            if (t->root != nullptr)
            {
                temp->root->children[0] = t->root;
            }
        }
    }
    return temp;
}

//expression function
syntaxTree * expression(void)
{
    syntaxTree * temp, *newtemp;

    temp = simple_expression();

    if (token.type == LT || token.type == EQUAL)
    {
        //create an op node
        newtemp = createTree();
        newtemp->root->ntype = expr;
        newtemp->root->etype = op;
        newtemp->root->children[0] = nullptr;
        newtemp->root->children[1] = nullptr;
        newtemp->root->children[2] = nullptr;
        newtemp->root->sibling = nullptr;

        //assign node's children
        if (newtemp != nullptr && temp!= nullptr)
        {
            if (newtemp->root != nullptr && temp->root != nullptr)
            {
                newtemp->root->children[0] = temp->root;
                temp = newtemp;
            }
        }

        match(token.type);
        if (newtemp != nullptr)
        {
            syntaxTree * t = simple_expression();
            if (t != nullptr)
            {
                newtemp->root->children[1] = t->root;
            }
        }
    }
    return temp;
}

//simple expression function
syntaxTree * simple_expression(void)
{
    syntaxTree *temp, *newtemp;

    temp = term();

    while (token.type == PLUS || token.type == MINUS)
    {
        switch (token.type)
        {
        case PLUS:
            match(PLUS);

            //create an op node
            newtemp = createTree();
            newtemp->root->ntype = expr;
            newtemp->root->etype = op;
            newtemp->root->children[0] = nullptr;
            newtemp->root->children[1] = nullptr;
            newtemp->root->children[2] = nullptr;
            newtemp->root->sibling = nullptr;

            //assign children
            if (newtemp != nullptr)
            {
                newtemp->root->children[0] = temp->root;
                newtemp->root->children[1] = term()->root;
            }

            temp = newtemp;
            break;
        case MINUS:
            match(MINUS);

            //create an op node
            newtemp = createTree();
            newtemp->root->ntype = expr;
            newtemp->root->etype = op;
            newtemp->root->children[0] = nullptr;
            newtemp->root->children[1] = nullptr;
            newtemp->root->children[2] = nullptr;
            newtemp->root->sibling = nullptr;

            //assign children
            if (newtemp != nullptr)
            {
                newtemp->root->children[0] = temp->root;
                newtemp->root->children[1] = term()->root;
            }

            temp = newtemp;
            break;
        }
    }
    return temp;
}

//term function
syntaxTree * term(void)
{
    syntaxTree *temp, *newtemp;

    temp = factor();

    while (token.type == MUL || token.type == SLASH)
    {
        switch (token.type)
        {
        case MUL:
            match(MUL);

            //create an op node
            newtemp = createTree();
            newtemp->root->ntype = expr;
            newtemp->root->etype = op;
            newtemp->root->children[0] = nullptr;
            newtemp->root->children[1] = nullptr;
            newtemp->root->children[2] = nullptr;
            newtemp->root->sibling = nullptr;

            //assign children
            if (newtemp != nullptr)
            {
                newtemp->root->children[0] = temp->root;
                newtemp->root->children[1] = factor()->root;
            }

            temp = newtemp;
            break;
        case SLASH:
            match(SLASH);

            //create an op node
            newtemp = createTree();
            newtemp->root->ntype = expr;
            newtemp->root->etype = op;
            newtemp->root->children[0] = nullptr;
            newtemp->root->children[1] = nullptr;
            newtemp->root->children[2] = nullptr;
            newtemp->root->sibling = nullptr;

            //assign children
            if (newtemp != nullptr)
            {
                newtemp->root->children[0] = temp->root;
                newtemp->root->children[1] = factor()->root;
            }

            temp = newtemp;
            break;
        }
    }
    return temp;
}

//factor function
syntaxTree * factor(void)
{
    syntaxTree *temp = nullptr;

    switch (token.type)
    {
    case LB:
        match(LB);
        temp = expression();

        match(RB);
        break;
    case NUM:
        match(NUM);

        //create a constant expression node
        temp = createTree();
        temp->root->ntype = expr;
        temp->root->etype = constant;
        temp->root->children[0] = nullptr;
        temp->root->children[1] = nullptr;
        temp->root->children[2] = nullptr;
        temp->root->sibling = nullptr;
        break;
    case ID:
        match(ID);

        //create an identifier expression node
        temp = createTree();
        temp->root->ntype = expr;
        temp->root->etype = identifier;
        temp->root->children[0] = nullptr;
        temp->root->children[1] = nullptr;
        temp->root->children[2] = nullptr;
        temp->root->sibling = nullptr;
        break;
    default:
        error(token.type,2);
        break;
    }

    return temp;
}
