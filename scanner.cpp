#include "scanner.h"

string input;               //input string
int counter =0;            //for string indexing

//functio nto check if colon
bool isColon(char c)
{
    if (c == ':')
    {
        return true;
    }
    return false;
}

//function to check if brackets
int isBracket(char c)
{
    if (c == '{')                   // left bracket
    {
        return 1;
    }
    else if (c == '}')             // right bracket
    {
        return 2;
    }
    return 0;                     // not bracket
}

//function to check if char is equal
bool isEqual(char c)
{
    if (c == '=')
    {
        return true;
    }
    return false;
}

//function to check if the token is a reserved word
int isReserved(string s)
{
    if (!s.compare("if"))
    {
        return 1;
    }
    else if (!s.compare("then"))
    {
        return 2;
    }
    else if (!s.compare("else"))
    {
        return 3;
    }
    else if (!s.compare("end"))
    {
        return 4;
    }
    else if (!s.compare("repeat"))
    {
        return 5;
    }
    else if (!s.compare("until"))
    {
        return 6;
    }
    else if (!s.compare("read"))
    {
        return 7;
    }
    else if (!s.compare("write"))
    {
        return 8;
    }
    else
    {
        return -1;
    }
}

//function to check if special symbol
int isSpecial(string s)
{
    if (!s.compare("+"))
    {
        return 1;
    }
    else if (!s.compare("-"))
    {
        return 2;
    }
    else if (!s.compare("*"))
    {
        return 3;
    }
    else if (!s.compare("/"))
    {
        return 4;
    }
    else if (!s.compare("="))
    {
        return 5;
    }
    else if (!s.compare("<"))
    {
        return 6;
    }
    else if (!s.compare("("))
    {
        return 7;
    }
    else if (!s.compare(")"))
    {
        return 8;
    }
    else if (!s.compare(";"))
    {
        return 9;
    }
    else
    {
        return -1;
    }
}

//get token function
TokenRecord getToken(void)
{
    State s = START;      //for the current state
    State ps = START;     //for previous state

    TokenRecord r;        //token record to be returned

    string temp;          //temporary char array to store string values

    while (s != DONE)
    {
        //start state
        if (s == START)
        {
            //if white space
            if (isspace(input[counter]))
            {
                s = START;
                //increment counter
                counter++;
            }
            //if digit
            else if (isdigit(input[counter]))
            {
                s = INNUM;
                temp = temp + input[counter];
                //increment counter
                counter++;
            }
            //if letter
            else if (isalpha(input[counter]))
            {
                s = INID;
                temp = temp + input[counter];
                //increment counter
                counter++;
            }
            //if :
            else if (isColon(input[counter]))
            {
                s = INASSIGN;
                temp = temp + input[counter];
                //increment counter
                counter++;
            }
            //if {
            else if (isBracket(input[counter]) == 1)
            {
                s = INCOMMENT;
                //increment counter
                counter++;
            }
            //if other (special symbols)
            else
            {
                temp = temp + input[counter];
                s = DONE;
                counter++;
            }
        }

        //incomment state
        else if (s == INCOMMENT)
        {
            if (isBracket(input[counter]) == 2)
            {
                s = START;
                //increment counter
                counter++;
            }
            else
            {
                s = INCOMMENT;
                //increment counter
                counter++;
            }
        }

        //innum state
        else if (s == INNUM)
        {
            ps = INNUM;
            if (isdigit(input[counter]))
            {
                s = INNUM;
                temp = temp + input[counter];
                //increment counter
                counter++;
            }
            else
            {
                s = DONE;
            }
        }

        //inid state
        else if (s == INID)
        {
            ps = INID;
            if (isalpha(input[counter]))
            {
                s = INID;
                temp = temp + input[counter];
                //increment counter
                counter++;
            }
            else
            {
                s = DONE;
            }
        }

        //inassign state
        else if (s == INASSIGN)
        {
            ps = INASSIGN;
            if (isEqual(input[counter]))
            {
                s = DONE;
                temp = temp + input[counter];
                counter++;
            }
            else
            {
                s = DONE;
            }
        }
    }

    //done state

    if (ps == INID)
    {
        //check if reserved word (token type only)
        if (isReserved(temp) != -1)
        {
            int x = isReserved(temp);
            if (x == 1)
            {
                r.type = IF;
            }
            else if (x == 2)
            {
                r.type = THEN;
            }
            else if (x == 3)
            {
                r.type = ELSE;
            }
            else if (x == 4)
            {
                r.type = END;
            }
            else if (x == 5)
            {
                r.type = REPEAT;
            }
            else if (x == 6)
            {
                r.type = UNTIL;
            }
            else if (x == 7)
            {
                r.type = READ;
            }
            else if (x == 8)
            {
                r.type = WRITE;
            }
        }
        //if not reserved word add string value (token type + string value )
        else
        {
            r.type = ID;
            r.strval = temp;
        }
    }

    else if (ps == INNUM)
    {
        //token type is number
        r.type = NUM;
        //add value to number value
        r.numval = stoi(temp);
    }

    else if (ps == INASSIGN)
    {
        if (!temp.compare(":="))
        {
            //token type is assignment
            r.type = ASSIGN;
        }
    }

    else if (ps == START)
    {
        //check if special symbol
        if (isSpecial(temp) != -1)
        {
            int x = isSpecial(temp);
            if (x == 1)
            {
                r.type = PLUS;
            }
            else if (x == 2)
            {
                r.type = MINUS;
            }
            else if (x == 3)
            {
                r.type = MUL;
            }
            else if (x == 4)
            {
                r.type = SLASH;
            }
            else if (x == 5)
            {
                r.type = EQUAL;
            }
            else if (x == 6)
            {
                r.type = LT;
            }
            else if (x == 7)
            {
                r.type = LB;
            }
            else if (x == 8)
            {
                r.type = RB;
            }
            else if (x == 9)
            {
                r.type = SEMIC;
            }
        }
    }

    if (counter >= input.length() + 1)
    {
        r.type = eof;
    }

    return r;
}

//function to get string of token type
QString getTokenType(TokenType tt)
{
    if (tt == 0)
    {
        return "IF";
    }
    else if (tt == 1)
    {
        return "THEN";
    }
    else if (tt == 2)
    {
        return "ELSE";
    }
    else if (tt == 3)
    {
        return "END";
    }
    else if (tt == 4)
    {
        return "REPEAT";
    }
    else if (tt == 5)
    {
        return "UNTIL";
    }
    else if (tt == 6)
    {
        return "READ";
    }
    else if (tt == 7)
    {
        return "WRITE";
    }
    else if (tt == 8)
    {
        return "PLUS";
    }
    else if (tt == 9)
    {
        return "MINUS";
    }
    else if (tt == 10)
    {
        return "MUL";
    }
    else if (tt == 11)
    {
        return "SLASH";
    }
    else if (tt == 12)
    {
        return "EQUAL";
    }
    else if (tt == 13)
    {
        return "LT";
    }
    else if (tt == 14)
    {
        return "LB";
    }
    else if (tt == 15)
    {
        return "RB";
    }
    else if (tt == 16)
    {
        return "SEMIC";
    }
    else if (tt == 17)
    {
        return "ASSIGN";
    }
    else if (tt == 18)
    {
        return "ID";
    }
    else if (tt == 19)
    {
        return "NUM";
    }
}
