#ifndef PARSER_H
#define PARSER_H

#include "scanner.h"
#include <QList>
#include <QVector>
#include <QRect>
#include<QLine>

//global variable

//current token
extern TokenRecord token;              //assigned by calling the function getToken

extern bool ms;
extern bool endf;

extern QList<TokenRecord> tokenlist;   //for tokens table
extern QList<QString> errorlist;       //for syntax errors field

extern QVector<QRect> rectangles;      //rectangles to be drawn for syntax tree
extern QVector<QRect> circles;         //circles to be drawn for syntax tree
extern QVector<QLine> lines;           //lines to be drawn for syntax tree

extern QList<QString> statements;      //statements type to be drawn on rectangles
extern QList<QString> expressions;     //expression type to be drawn on circles

extern int startX , startY;

extern QString of;                     //string to be output in file


//enums

//type of node
typedef enum {stmt , expr}nodeType;      //expression node or statement node

//type of statements
typedef enum {ifStmt, repeatStmt , assignStmt , readStmt , writeStmt}stmtType;    //5 types of statements

//types of expressions
typedef enum {op, constant , identifier}expType;    //3 types of expressions

//structures

//tree node
typedef struct node
{
    struct node * children[3];				//array of pointers to children
    struct node * sibling;					//pointer to sibling (next stmt or expression)

    nodeType ntype;                        // node type (statement / exp )
    stmtType stype;                        // statament type
    expType  etype;                        // expression type

    int x;                                  // x coordinate for drawing
    int y;                                 // y  coordinate for drawing

}treeNode;

//syntax tree
typedef struct tree
{
    treeNode * root;                       // root of syntax tree
}syntaxTree;

//output tree
extern syntaxTree *tree;

//functions

QString getnodeType(treeNode * node);

//Tree traversal
void Traverse(treeNode * node);

//function to create syntax tree
syntaxTree * createTree(void);

//error function
void error(TokenType tt, int i);

//match function
bool match(TokenType ttype);

//EBNF implementation

//factor function
syntaxTree * factor(void);

//term function
syntaxTree * term(void);

//simple expression function
syntaxTree * simple_expression(void);

//expression function
syntaxTree * expression(void);

//write statement function
syntaxTree * writeStatement(void);

//read statement function
syntaxTree * readStatement(void);

//assignment statement function
syntaxTree * assignStatament(void);

//repeat statement function
syntaxTree * repeatStatement(void);

//if statement function
syntaxTree * ifStatement(void);

//statement function
syntaxTree * statement(void);

//statement sequence function
syntaxTree * stmtSequence(void);

//program function
syntaxTree * Program(void);

#endif // PARSER_H
