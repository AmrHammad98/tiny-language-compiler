#ifndef SCANNER_H
#define SCANNER_H

#include "stdio.h"
#include <iostream>
#include <cctype>
#include <string>
#include <stdlib.h>
#include <QString>

using namespace std;

extern string input;               //input string
extern int counter;            //for string indexing

// States of scanner
typedef enum { START, INNUM, INID, INASSIGN, INCOMMENT, DONE }State;

//type of tokens
typedef enum { IF, THEN, ELSE, END, REPEAT, UNTIL, READ, WRITE, PLUS, MINUS, MUL, SLASH, EQUAL, LT, LB, RB, SEMIC, ASSIGN, ID, NUM,eof }TokenType;

//token record
typedef struct
{
    TokenType type;
    string strval;             //needed only for identifiers
    int numval;               //needed only for numbers
}TokenRecord;

//functio nto check if colon
bool isColon(char c);

//function to check if brackets
int isBracket(char c);

//function to check if char is equal
bool isEqual(char c);

//function to check if the token is a reserved word
int isReserved(string s);

//function to check if special symbol
int isSpecial(string s);

//get token function
TokenRecord getToken(void);

QString getTokenType(TokenType tt);



#endif // SCANNER_H
