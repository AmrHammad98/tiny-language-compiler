#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
//#include <QtGui>
//#include <QtCore>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Browse_Btn_clicked();

    void on_cmpl_Btn_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QGraphicsEllipseItem *ellipse;
    QGraphicsRectItem *rect;
    QGraphicsTextItem *text;
    QGraphicsLineItem *line;

protected:
    void paintEvent (QPaintEvent *e);
/*protected:
    void drawForeground(QPainter* painter, const QRectF& rect);*/
};

#endif // MAINWINDOW_H
