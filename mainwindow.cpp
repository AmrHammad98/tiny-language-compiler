#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "scanner.cpp"
#include "parser.cpp"


#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include<QString>
#include<QColor>
#include <QPainter>
#include <QGraphicsItem>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void WriteFile(QString Filename , QString s)
{
    QFile ofile(Filename);

    if(!ofile.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::information(nullptr,"info",ofile.errorString());
    }

    QTextStream out (&ofile);
    out <<s;

    ofile.flush();
    ofile.close();

}

void MainWindow::on_Browse_Btn_clicked()
{
    ui->Input_field->clear();

    QString filename = QFileDialog::getOpenFileName
            (this , tr("Browse File") , "C://Desktop", "Text File *.txt");

    QFile file (filename);
    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(nullptr,"info",file.errorString());
    }

    QTextStream in (&file);
    ui->Input_field->setText(in.readAll());

    file.close();
}

void MainWindow::on_cmpl_Btn_clicked()
{
    input = ui->Input_field->toPlainText().toStdString();
    ui->Error_field->setReadOnly(true);

    ui->Error_field->clear();

    //ui->Token_table->clear();

    if(tokenlist.isEmpty() == false)
    {
        tokenlist.clear();
    }

    if(errorlist.isEmpty() == false)
    {
        errorlist.clear();
    }

    if(statements.isEmpty() == false)
    {
        statements.clear();
    }

    if(expressions.isEmpty() == false)
    {
        expressions.clear();
    }

    if(rectangles.isEmpty() == false)
    {
        rectangles.clear();
    }

    if(circles.isEmpty() == false)
    {
        circles.clear();
    }


    if(lines.isEmpty() == false)
    {
        lines.clear();
    }



    tree = Program();
    //Program();
    int r = 0;
    //fill table
    for (int i = 0 ; i< tokenlist.count() -1 ;i++)
    {
        ui->Token_table->insertRow(ui->Token_table->rowCount());
        r  = ui->Token_table->rowCount() -1;

        QString type = getTokenType(tokenlist.at(i).type);
        QString val = val.fromStdString(tokenlist.at(i).strval);

        ui->Token_table->setItem(r, 0, new QTableWidgetItem(val));
        ui->Token_table->setItem(r,1,new QTableWidgetItem(type));
    }

    //show syntax errors if found
    ui->Error_field->setTextColor(QColor(255,0,0));
    foreach(QString s , errorlist)
    {
        ui->Error_field->append(s);
    }

    //draw syntax tree
    Traverse(tree->root);

    WriteFile("tiny_out.txt",of);
}

void MainWindow :: paintEvent(QPaintEvent *e)
{
    QPainter painter(this);

    painter.drawRects(rectangles);

    painter.drawLines(lines);


    foreach(QRect r , circles)
    {
        painter.drawEllipse(r);
    }

    int i = 0;
    foreach(QRect r , circles)
    {
        painter.drawText(r,Qt::AlignCenter,expressions.at(i));
        i++;
    }
    i = 0;
    foreach(QRect r , rectangles)
    {
        painter.drawText(r,Qt::AlignCenter,statements.at(i));
        i++;
    }
}


